

function cb(){
    console.log("This is a peek inside callback.");
}


function limitFunctionCallCount(cb, n){

    let counter = 0;

    return function callsCB(){            // This function forms a closure with parent function and a calls cb function.

        if (counter < n){       // Check to see if counter condition is met.
            
            counter++;          
            cb();
        
        }else{
            console.log(` Can't run callback, it has already been called ${n} times.`);
        }
    }
}


module.exports.limitFunctionCallCount = limitFunctionCallCount;
module.exports.cb = cb;