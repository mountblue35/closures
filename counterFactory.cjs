


function counterFactory(){

    let counter = 5;                // counter variable 

    function increment(){            // Increment function forming a closure with counterFactory
         
        ++counter;

        // console.log("inside inc function : ",counter);
        return counter;
    }

    function decrement(){            // Decrement func. forming a closure with counterFactory

        --counter;
        
        // console.log("inside dec function : ",counter);
        return counter;
    }

    return {increment : increment, decrement: decrement}        // Returns object containing methods
}


module.exports = counterFactory;