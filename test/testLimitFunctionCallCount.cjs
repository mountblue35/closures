

const second = require('../limitFunctionCallCount.cjs');

let limit = 3;

let result = second.limitFunctionCallCount(second.cb, limit);
result();
result();
result();       // Allowed till here (3 times).
result();       // Over limit.
result();       // Over limit.