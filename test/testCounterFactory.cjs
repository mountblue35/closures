

const first = require('../counterFactory.cjs');

const result = first();

let tester;

tester = result.increment();                                    // Increments the counter variable, stores in tester
console.log("After Increment : ",tester);                       // Logs the result after running increment method 

tester = result.decrement();                                    // Decrements the counter variable, stores in tester
console.log("After Decrement:", tester);                       // Logs the result after running increment method

tester = result.decrement();                                    // Decrements the counter variable, stores in tester
console.log("After Decrement:", tester);                       // Logs the result after running increment method

tester = result.increment();                                    // Increments the counter variable, stores in tester    
console.log("After Increment : ",tester);                       // Logs the result after running increment method 
