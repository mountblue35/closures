

function cb(){
    return "Welcome to callback";           
}


function cacheFunction(cb){

    let cache = {};

    return function callsCB(...para){            // This function forms a closure with parent function and a calls cb function.

        if (cache[para] == undefined ){         // This checks if set of argument is present in cache already.

            cache[para] = para;                 // adds arguments in cache object.

            return cb();                        // calls callback function
            
        }else{ 

            return cache;                   // returns cache object
        }
    }
}


module.exports.cacheFunction = cacheFunction;
module.exports.cb = cb;